/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21-May-2017 08:37:13                        ---
 * ----------------------------------------------------------------
 */
package com.flintgroup.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFlintyb2bFacadesConstants
{
	public static final String EXTENSIONNAME = "flintyb2bfacades";
	
	protected GeneratedFlintyb2bFacadesConstants()
	{
		// private constructor
	}
	
	
}
