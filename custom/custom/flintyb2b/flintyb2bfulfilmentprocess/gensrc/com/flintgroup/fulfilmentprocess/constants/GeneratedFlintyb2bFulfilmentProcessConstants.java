/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21-May-2017 08:37:13                        ---
 * ----------------------------------------------------------------
 */
package com.flintgroup.fulfilmentprocess.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFlintyb2bFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "flintyb2bfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedFlintyb2bFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
