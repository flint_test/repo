/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 4, 2017 11:34:45 AM                     ---
 * ----------------------------------------------------------------
 */
package com.flintgroup.cockpits.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFlintyb2bCockpitsConstants
{
	public static final String EXTENSIONNAME = "flintyb2bcockpits";
	
	protected GeneratedFlintyb2bCockpitsConstants()
	{
		// private constructor
	}
	
	
}
